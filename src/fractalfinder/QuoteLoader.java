/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalfinder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author lrisinger
 */
public class QuoteLoader
{

    public QuoteLoader(File quotes)
    {
        this.quotes = quotes;
    }

    public List<String> getSymbolList()
    {
        
        List<String> list = new ArrayList<String>();
        list.addAll(this.symbol2quoteListMap.keySet());
        java.util.Collections.sort(list);
        return list;
    }
    
    List<Quote> getQuoteList(String symbol)
    {
        return this.symbol2quoteListMap.get(symbol);
    }
    
    
    public void load()
    {
        try
        {
            
            DateFormat df = new SimpleDateFormat("yyyyMMdd");
            
            
            if(quotes.exists() && quotes.canRead())
            {
                
                this.symbol2quoteListMap.clear();
                
                BufferedReader br = new BufferedReader(new FileReader(quotes));
                String line;
                while ((line = br.readLine()) != null) 
                {
                    if(line.contains("#") )
                    {
                        // skipp comments
                        continue;
                    }
                   // process the line.
                   // # Date, Ticker, Open, High, Low, Close, Volume
                   // 20090821,A,25.6,25.61,25.22,25.55,34758
                   String[] vals = line.split(",");
                   
                   Quote qt = new Quote( df.parse(vals[0]), 
                           vals[1], 
                           Float.parseFloat(vals[2]), 
                           Float.parseFloat(vals[3]), 
                           Float.parseFloat(vals[4]), 
                           Float.parseFloat(vals[5]),
                           Integer.parseInt(vals[6]));
                   
                   
                   
                   List<Quote> qtList = null;
                   if(symbol2quoteListMap.containsKey(qt.getSymbol()))
                   {
                      qtList = symbol2quoteListMap.get(qt.getSymbol());
                      Integer day = symbol2DayMap.get(qt.getSymbol()); 
                      qt.setDay( day++ );
                      symbol2DayMap.put(qt.getSymbol(), day);
                      
                   }
                   else
                   {
                       qtList = new ArrayList<Quote>();
                       symbol2quoteListMap.put(qt.getSymbol() , qtList);
                       symbol2DayMap.put(qt.getSymbol(), 0);
                       qt.setDay(0);
                   }
                   qtList.add(qt);
                }
                br.close();
            }
            else
            {
                System.out.println("QuoteLoader: failed to load file: "+quotes.getPath());
            }
        } 
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    
    private File quotes;
    private Map<String, List<Quote> > symbol2quoteListMap = new HashMap<String, List<Quote> >();
    private Map<String, Integer > symbol2DayMap = new HashMap<String, Integer >();
    
}
