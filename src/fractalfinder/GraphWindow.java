/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalfinder;

import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import org.jfree.chart.ChartFactory; 
import org.jfree.chart.ChartPanel; 
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.annotations.XYShapeAnnotation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.general.SeriesException; 
import org.jfree.data.time.Day; 
import org.jfree.data.time.TimeSeries; 
import org.jfree.data.time.TimeSeriesCollection; 
import org.jfree.data.xy.XYDataset; 
import org.jfree.ui.ApplicationFrame; 



public class GraphWindow extends ApplicationFrame 
{
   public GraphWindow( final String title )
   {
      super( title );         
   }
   
   public void drawGraph()
   {
//      final XYDataset dataset = createDataset( );          
      final XYDataset dataset = createStockDataset( );         
      final JFreeChart chart = createChart( dataset );
//      Ellipse2D.Double circle = new Ellipse2D.Double(20, 0, 20, 20);
//      chart.getXYPlot().addAnnotation(new XYShapeAnnotation(circle));
//      chart.getXYPlot().getRenderer().setBaseShape(circle);
      XYLineAndShapeRenderer rend = (XYLineAndShapeRenderer) chart.getXYPlot().getRenderer();
      int seriesCnt = chart.getXYPlot().getSeriesCount();
      for( int i = 1; i < seriesCnt; ++i)
      {
        rend.setSeriesShapesVisible(i, true);
        rend.setSeriesItemLabelsVisible(i, true);
      }

     
      final ChartPanel chartPanel = new ChartPanel( chart );  
      chartPanel.setPreferredSize( new java.awt.Dimension( 2*560 , 2*370 ) );         
      chartPanel.setMouseZoomable( true , false ); 
      setContentPane( chartPanel );
   }

   private XYDataset createDataset( ) 
   {
      final TimeSeries series = new TimeSeries( "Random Data" );         
      Day current = new Day( );         
      double value = 100.0;         
      for (int i = 0; i < 4000; i++)    
      {
         try 
         {
            value = value + Math.random( ) - 0.5;                 
            series.add(current, new Double( value ) );                 
            current = ( Day ) current.next( ); 
         }
         catch ( SeriesException e ) 
         {
            System.err.println("Error adding to series");
         }
      }

      return new TimeSeriesCollection(series);
   }    
   
   private XYDataset createStockDataset( ) 
   {
      final TimeSeries series = new TimeSeries( this.stockName );         
      for(Quote q : this.qList)
      {
         try 
         {
            Day day = new Day(q.getDate()); 
            series.add(day, new Double( q.getAverage() ) );                 
         }
         catch ( SeriesException e ) 
         {
            System.err.println("Error adding to series");
         } 
      }
      TimeSeriesCollection tsc = new TimeSeriesCollection(series);
      int cnt = 0;
      for(FractalRecord fr : fractals)
      {
//          if(cnt > 2)  break; // only do one
          final TimeSeries fSeries = new TimeSeries( "fractal-"+fr.status.toString() ); 
          fSeries.add(new Day(fr.start.getDate()), new Double(fr.start.getAverage())  );
          fSeries.add(new Day(fr.a.getDate()), new Double(fr.a.getAverage())  );
          fSeries.add(new Day(fr.b.getDate()), new Double(fr.b.getAverage())  );
          if(fr.A != null) fSeries.add(new Day(fr.A.getDate()), new Double(fr.A.getAverage())  );
          if(fr.B != null) fSeries.add(new Day(fr.B.getDate()), new Double(fr.B.getAverage())  );
          if(fr.C != null) fSeries.add(new Day(fr.C.getDate()), new Double(fr.C.getAverage())  );
          tsc.addSeries(fSeries);
          cnt++;
      }
      
      
      return tsc; 
   }     

   private JFreeChart createChart( final XYDataset dataset ) 
   {
      return ChartFactory.createTimeSeriesChart(             
      "Stock: "+this.stockName, 
      "Day",              
      "Avg Daily Price",              
      dataset,             
      true,              
      true,              
      false);
   }

//   public static void main( final String[ ] args )
//   {
//      final String title = "Time Series Management";         
//      final GraphWindow demo = new GraphWindow( title );         
//      demo.pack( );         
//      RefineryUtilities.centerFrameOnScreen(demo);
//      demo.setVisible( true );
//   }
   
   public void setQuoteList(List<Quote> qList)
   {
       this.qList = qList;
       this.stockName = qList.get(0).getSymbol();
   }
   public void add(FractalRecord fractal) 
   {
       fractals.add(fractal); 
   }
           
   
   
   private List<Quote> qList;
   private String stockName;
   private List<FractalRecord> fractals = new ArrayList<FractalRecord>();
} 
