/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalfinder;

import java.util.Objects;

/**
 *
 * @author lrisinger
 */
public class FractalRecord
{
    public enum FractalStatus {NONE, PARTIAL, COMPLETE, ACTIVE, FAILED};
    
    public Quote start = null;
    public Quote a = null;
    public Quote b = null;
    public Quote c = null;
    public Quote A = null;
    public Quote B = null;
    public Quote C = null;
    public float b_over_a = 0;
    public float c_over_a = 0;
    public float length_A = 0;
    public float length_B = 0;
    public FractalStatus status = FractalStatus.NONE;
    public void calcRatios()
    {
        float length_a = java.lang.Math.abs(a.getAverage() - start.getAverage());
        float length_b = java.lang.Math.abs(b.getAverage() - a.getAverage());
        float length_c = java.lang.Math.abs(c.getAverage() - b.getAverage());
              length_A = java.lang.Math.abs(c.getAverage() - start.getAverage());
        b_over_a = length_b/length_a;
        c_over_a = length_c/length_a;
    }
    public void calcBLength()
    {
        if(length_B == 0)
        {
            length_B = java.lang.Math.abs(c.getAverage() - B.getAverage());
        }
    }
    
    public boolean isFractal_abAB(float confidence)
    {
        boolean rc = false;
        if(b_over_a > 0 && java.lang.Math.abs(length_A) > 0 && java.lang.Math.abs(length_B) > 0)
        {
            // compare to confidence using percent difference
            float B_over_A = length_B/length_A;
            float absDiff = java.lang.Math.abs( B_over_A - b_over_a);
            float avg = (B_over_A + b_over_a)/2;
            float percentDiff = absDiff/avg; // on 0 to 1 percent
            rc = percentDiff <= (1.0 - confidence);
    //        System.out.println("fractal in abAB percentDiff: " + percentDiff);
            if(rc == true)
            {
//                System.out.println("fractal in abAB symbol: "+ this.a.getSymbol() + " confidence: " + (1.0f - percentDiff) );
            }
        }
        return rc;
    }
    
    public boolean isFractal_acAC(float confidence)
    {
        boolean rc = false;
        float length_C = C.getAverage() - B.getAverage();
        if(c_over_a > 0 && java.lang.Math.abs(length_A) > 0 && java.lang.Math.abs(length_C) > 0)
        {
            // compare to confidence using percent difference
            float C_over_A = length_C/length_A;
            float absDiff = java.lang.Math.abs( C_over_A - c_over_a);
            float avg = (C_over_A + c_over_a)/2;
            float percentDiff = absDiff/avg; // on 0 to 1 percent
            rc = percentDiff <= (1.0 - confidence);
//            System.out.println("fractal in acAC percentDiff: " + percentDiff);
            if(rc == true)
            {
//                System.out.println("fractal in acAC symbol: "+ this.a.getSymbol() + " confidence: " + (1.0f - percentDiff) );
            }
        }
        return rc;
    }

    @Override
    public String toString() {
        return "FractalRecord{" + "start=" + start + ", a=" + a + ", b=" + b + ", c=" + c + ", A=" + A + ", B=" + B + ", C=" + C + ", b_over_a=" + b_over_a + ", c_over_a=" + c_over_a + ", length_A=" + length_A + ", length_B=" + length_B + ", status=" + status + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.start);
        hash = 37 * hash + Objects.hashCode(this.a);
        hash = 37 * hash + Objects.hashCode(this.b);
        hash = 37 * hash + Objects.hashCode(this.c);
        hash = 37 * hash + Objects.hashCode(this.A);
        hash = 37 * hash + Objects.hashCode(this.B);
        hash = 37 * hash + Objects.hashCode(this.C);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FractalRecord other = (FractalRecord) obj;
        if (!Objects.equals(this.start, other.start)) {
            return false;
        }
        if (!Objects.equals(this.a, other.a)) {
            return false;
        }
        if (!Objects.equals(this.b, other.b)) {
            return false;
        }
        if (!Objects.equals(this.c, other.c)) {
            return false;
        }
        if (!Objects.equals(this.A, other.A)) {
            return false;
        }
        if (!Objects.equals(this.B, other.B)) {
            return false;
        }
        if (!Objects.equals(this.C, other.C)) {
            return false;
        }
        return true;
    }
    
    
}
