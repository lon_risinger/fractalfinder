/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalfinder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jfree.chart.JFreeChart;  // tutorial: https://www.tutorialspoint.com/jfreechart/jfreechart_timeseries_chart.htm

/**
 *
 * @author lrisinger
 */
public class PatternFinder
{
    public enum PatternState {START, FOUND_a, FOUND_b, FOUND_A, FOUND_B, FOUND_C, FAILED, ACTIVE, COMPLETE }; 
    private Quote m_start = null;
    private QuoteSection m_section = new QuoteSection();
    private List<FractalRecord> m_partial = new ArrayList<FractalRecord>();
    private List<FractalRecord> m_complete = new ArrayList<FractalRecord>();
    private List<FractalRecord> m_active = new ArrayList<FractalRecord>();
    private List<FractalRecord> m_failed = new ArrayList<FractalRecord>();

    public List<FractalRecord> getPartial()
    {
        return m_partial;
    }

    public List<FractalRecord> getComplete()
    {
        return m_complete;
    }

    public List<FractalRecord> getActive() {
        return m_active;
    }

    public List<FractalRecord> getFailed() {
        return m_failed;
    }
    
    
    public void clear() 
    {
        m_partial.clear();
        m_complete.clear();
        m_active.clear();
        m_failed.clear();
    }
    /**
     * 
     * @param quoteList - list of quotes for a single stock symbol 
     */
//    void search(List<Quote> quoteList)
//    {
//        clear();
//        List<FractalRecord> found = new ArrayList<FractalRecord>();
//        List<Quote> qList = new ArrayList(quoteList);
//        int offset = 0;
//        int size = qList.size() -1;
//        boolean breakWhile = false;
//        PatternState state = PatternState.START;
//        m_start = qList.get(0);
//        FractalRecord fractal = new FractalRecord();
//        fractal.start = m_start;
//        m_section.add(m_start);
//        String symbol = m_start.getSymbol();
//        Iterator<Quote> iter = qList.iterator();
//        while(offset < size - 3)
//        {
//            while(iter.hasNext()  )
//            {
//                Quote curr = iter.next();
//                if(curr == m_start) continue;
//
//                switch(state)
//                {
//                    case START:
////                        System.out.println("state == start");
//                        m_section.add(curr);
//                        if(m_section.size() > 4)
//                        {
//                            // look for point A  ( curr < A, A is max on (start, curr)
//                            Quote max = m_section.getMax();
//                            if( curr != max && curr.getAverage() < max.getAverage() )
//                            {
////                                System.out.println("Symbol: "+curr.getSymbol() + " - Found a" );
//                                state = PatternState.FOUND_a;
//                                fractal.a = curr;
//                            }
//
//                        }
//                        break;
//                   case FOUND_a:
//                        m_section.add(curr);
//                        // keep going until curr > a  then b = min on (a, end)
//                        if(curr.getAverage() > fractal.a.getAverage() )
//                        {
//                            Quote tmp = m_section.getMinOn(fractal.a, curr);
//                            fractal.b = tmp;
//                            state = PatternState.FOUND_b;
////                            System.out.println("Symbol: "+curr.getSymbol() + " - Found b" );
//
//                        }     
//                        break;
//                   case FOUND_b:
//                        // keep going while curr is increasing
//                        if( curr.getAverage() > m_section.last().getAverage())
//                        {
//                            m_section.add(curr);
//                        }
//                        else
//                        {
//                            m_section.add(curr);
//                            // curr not increasing c == max on (a, curr)
//                            fractal.c = m_section.getMaxOn(fractal.a, curr);
//                            fractal.A = fractal.c;
////                            System.out.println("Symbol: "+curr.getSymbol() + " - Found A" );
//                            fractal.calcRatios();
//                            state = PatternState.FOUND_A;
//                        }
//                        break;    
//                   case FOUND_A:
//                        if( curr.getAverage() < fractal.c.getAverage() )
//                        {
//                            m_section.add(curr);
//                        }
//                        else
//                        {
//                            // curr >= c
//                            // possible B 
//                            m_section.add(curr);
//                            Quote possB = m_section.getMinOn(fractal.A, curr);
//                            if(possB.getAverage() <= fractal.b.getAverage())
//                            {
//                                float possBLength = possB.getAverage() - fractal.A.getAverage();
//                                float expectedBLength = fractal.b_over_a * fractal.length_A;
//                                float diff = java.lang.Math.abs(possBLength - expectedBLength);
//                                if(diff <= 0.05 * expectedBLength)
//                                {
//                                    // 95% confidence
//                                    fractal.B = possB;
//                                    state = PatternState.FOUND_B;
//                                    fractal.status = FractalRecord.FractalStatus.PARTIAL;
//                                    fractal.calcBLength();
//                                    // here we know we have at least a partial fractal because curr >= c | A
//                                    found.add(fractal);
////                                    System.out.println("Symbol: "+curr.getSymbol() + " -  found B" );
//                                }
//                            }
//                            else
//                            {
////                                System.out.println("Symbol: "+curr.getSymbol() + " -  B failed !!!" );
//                            }
//                        }
//                        break;
//                     case FOUND_B:
//                         // parial fractial already found - does it complete
//                         m_section.add(curr);
////                         if(curr.getAverage() > fractal.c.getAverage())
////                         {
////                            // test for full C completeion
////                            float possCLength = curr.getAverage() - fractal.B.getAverage();
////                            float expectedCLength = fractal.c_over_a * fractal.length_A;
////                            float diff = java.lang.Math.abs(possCLength - expectedCLength);
////                            if(diff <= 0.05 * expectedCLength)
////                            {
////                                // 95% confidence
////                                fractal.C = curr;
////                                state = PatternState.FOUND_C;
////                                fractal.status = FractalRecord.FractalStatus.COMPLETE;
//////                                    System.out.println("Symbol: "+curr.getSymbol() + " -  found full C" );
////                            }
////                         }
//                         float projectedC = fractal.c_over_a * fractal.length_A + fractal.B.getAverage();
//                         if(curr.getAverage() >= projectedC )
//                         {
//                             if(m_section.getMinOn(fractal.B, m_section.last()) == fractal.B)
//                             {
//                                state = PatternState.FOUND_C;
//                                fractal.status = FractalRecord.FractalStatus.COMPLETE;
////                                System.out.println("Symbol: "+curr.getSymbol() + " -  found full C" );
//                             }
//                         }
//                         else if(curr.getAverage() < fractal.B.getAverage())
//                         {
//                             state = PatternState.FOUND_C;
////                             System.out.println("Symbol: "+curr.getSymbol() + " -  full C Failed !!!" );
//                         }
//
//                        break;
//
//                     case FOUND_C:    
//                         breakWhile = true;
//                         break;      
//                } // end switch
//                if(breakWhile) break;
//            } // end whle iter
//        
//            // reset While iter
//            breakWhile = false;
//            // reset fractal record
//            fractal = new FractalRecord();
//            // reset for another start == start + 1
//            offset++;
//            // ???? now what -- still thinking about how to reset list
//            qList = quoteList.subList(offset, size);
//            iter = qList.iterator();
//            m_start = qList.get(0);
//            fractal.start = m_start;
//            state = PatternState.START;
//            m_section.clear();
//            m_section.add(m_start);
//            
//        } // while offset
//        
//        // move all found fractals into appropriate lists
//        // save fractal record
//        Iterator<FractalRecord> it =found.iterator();
//        while(it.hasNext())
//        {
//            FractalRecord f = it.next();
//            if(f.status == FractalRecord.FractalStatus.PARTIAL)
//            {
//                this.m_partial.add(f);
//            }
//            else if(f.status == FractalRecord.FractalStatus.COMPLETE)
//            {
//                this.m_complete.add(f);
//            }
//        }
//        System.out.println("Symbol: "+symbol+" Found partial: "+ this.m_partial.size() + " compplete: "+this.m_complete.size() );
//    }
    
    void search2(List<Quote> quoteList)
    {
//        float confidence = 0.95f;
//        float confidence = 0.80f;
        float confidence = 0.99f;
        FractalRecord prevFrac = null;
        clear();
//        List<Quote> qList = new ArrayList(quoteList);
//        PatternState state = PatternState.START;
//        m_start = quoteList.get(0);
//        FractalRecord fractal = new FractalRecord();
//        fractal.start = m_start;
//        String symbol = m_start.getSymbol();
//        Iterator<Quote> iter = quoteList.iterator();
        
        int iterStart = 0;
        do
        {
            Iterator<Quote> iter = quoteList.listIterator(iterStart);
            PatternState state = PatternState.START;
            m_start = quoteList.get(iterStart);
            FractalRecord fractal = new FractalRecord();
            fractal.start = m_start;
            String symbol = m_start.getSymbol();
            
            QuoteSection qSection = new QuoteSection();
            while( iter.hasNext() )
            {
                Quote curr = iter.next();
                qSection.add(curr);
                // find lightning generator pattern
                if(findGeneratorPattern_ab(qSection, fractal, state))
                {
                    // found a generator
    //                this.m_partial.add(fractal);
                    List<FractalRecord> fracRecordsOut = new ArrayList<FractalRecord>();
                    if(findGeneratorPattern_AB(quoteList, fractal, state, fracRecordsOut))
                    {
                        Iterator<FractalRecord> frOutIter = fracRecordsOut.iterator();
                        while(frOutIter.hasNext())
                        {
                            fractal = frOutIter.next();
        //                    this.m_partial.add(fractal);
                            if(fractal.isFractal_abAB(confidence))
                            {
                                //System.out.println(fractal);   
                                //System.out.println("iterStart: "+iterStart+ " qSection size: " + qSection.size());
                                // at this point we know the pattern is fractal on abAB
                                // look for completeion of the pattern abcABC
                                if( findCompletePattern_BC(quoteList, fractal, state, confidence) )
                                {
                                    // check for fractal on acAC
                                    // check for duplcates - which is common
                                    if(!isDuplicate(prevFrac, fractal))
                                    {
                                        if(fractal.status == FractalRecord.FractalStatus.COMPLETE )
                                        {
                                            this.m_complete.add(fractal);
                                        }
                                        else if(fractal.status == FractalRecord.FractalStatus.ACTIVE)
                                        {
                                            this.m_active.add(fractal);
                                        }
                                    }
                                }
                                else if(fractal.status == FractalRecord.FractalStatus.FAILED)
                                {
                                    if(!isDuplicate(prevFrac, fractal))
                                    {
                                        this.m_failed.add(fractal);
                                    }
                                }
                                prevFrac = fractal;
                            }
                        }
                    }
                    // reset for next
                    fractal = new FractalRecord();
                    fractal.start = m_start;
                    state = PatternState.START;
                }
                

            }
            iterStart++;
            
        }
        while(iterStart < quoteList.size());
       

    }
    
    boolean findGeneratorPattern_ab(QuoteSection section, FractalRecord frac, PatternState state)
    {
        boolean rc = false;
        if(section.size() > 5)
        {
            Quote max = section.getMax(); 
            // if max is not the start or end of section
             if( max.getDay() > section.first().getDay() && max.getDay() < section.last().getDay())
             {
                 
                 // if start is the min value between start and a
                 Quote minOnStartToA = section.getMinOn(section.first(), max);
                 if(minOnStartToA.getAverage() >= section.first().getAverage() ) // implies lightning up
                 {
                    
                    // find min between max and end
                    state = PatternState.FOUND_a;
                    frac.a = max;
                    Quote min = section.getMinOn(max, section.last()); 
                    if( min.getDay() > max.getDay() && min.getDay() < section.last().getDay())
                    {
                       // min is between max and end
                       state = PatternState.FOUND_b;
                       frac.b = min; 
                       rc = true;
                    }
                 }
             }
        }
        
        return rc;
    }
    
    boolean findGeneratorPattern_AB(List<Quote> quoteList, FractalRecord fractal, PatternState state, List<FractalRecord> fracRecordsOut)
    {
        FractalRecord originalFrac = fractal;
        
        boolean rc = false;
        /* Already have a and b Look for A and B  */
        int bIndex = quoteList.indexOf(fractal.b);
        Iterator<Quote> iter = quoteList.listIterator(bIndex);
        
        QuoteSection qSection = new QuoteSection();
        
        int cnt = 0;
        while( iter.hasNext() )
        {
            Quote curr = iter.next();
            qSection.add(curr);
            // find lightning generator pattern
            if(findGeneratorPattern_AB_impl(qSection, fractal, state))
            {
               fractal.calcRatios();
               fractal.calcBLength();
               rc = true;
//               break;  //FIXME this may be blocking larger patterns
               // make a copy of the original ab pattern and keep going
               fracRecordsOut.add(fractal);
               fractal = new FractalRecord();
               fractal.start = originalFrac.start;
               fractal.a = originalFrac.a;
               fractal.b = originalFrac.b;
            }
            cnt++;
        }
        
        
        
        return rc; 
    }
    
    boolean findGeneratorPattern_AB_impl(QuoteSection section, FractalRecord frac, PatternState state)
    {
        
        // TODO
        boolean rc = false;
        if(section.size() > 3)
        {
            Quote max = section.getMax(); // section starts at b
            // max is not first (b) or last
             if( max.getDay() > section.first().getDay() && max.getDay() < section.last().getDay())
             {
                 // max is not the start or end of section
                 if(max.getAverage() > frac.a.getAverage()) // require up lightning pattern c > a ???? not sure about this
//                 if(true)
                 {
                    
                    if(section.getMinOn(section.first(), max).getDay() == frac.b.getDay() )
                    {
                        // There is not a lower point between 'b' and 'c'  i.e. min == b     
                        state = PatternState.FOUND_A;
                        frac.c = max;
                        frac.A = max;
                        // find min between max and end
                        Quote min = section.getMinOn(max, section.last()); 
                        if( min.getDay() > max.getDay() && min.getDay() < section.last().getDay())
                        {
                           // min is between max and end
                           state = PatternState.FOUND_B;
                           frac.B = min; 
                           rc = true;
                        }
                    }
                 }
                 
             }
        }
        
        return rc;
    }
    
    boolean findCompletePattern_BC(List<Quote> quoteList, FractalRecord fractal, PatternState state, float confidence)
    {
        boolean rc = false;
        FractalRecord originalFrac = fractal;
        
        /* Already have a and b and A and B Look C  */
        int BIndex = quoteList.indexOf(fractal.B);
        Iterator<Quote> iter = quoteList.listIterator(BIndex);
        
        QuoteSection qSection = new QuoteSection();
        
        int cnt = 0;
        while( iter.hasNext() )
        {
            Quote curr = iter.next();
            qSection.add(curr);
            // find lightning generator pattern
            if(findCompletePattern_BC_impl(qSection, fractal, state))
            {
                // found a possible C 
                // - if c/a == C/A == > COMPLETE fractal
                // - if C >= B (it is) and C is the end of the data then this pattern is still ACTIVE ($$$)
                if(fractal.isFractal_acAC(confidence))
                {
                   // complete 
                    //TODO
                   fractal.status = FractalRecord.FractalStatus.COMPLETE;
                   state = PatternState.COMPLETE;
                    
                    rc = true;
                }
                else if(fractal.C == quoteList.get(quoteList.size() - 1) )
                {
                    // active
                    fractal.status = FractalRecord.FractalStatus.ACTIVE;
                    state = PatternState.ACTIVE;
                    rc = true;
                }
            }
            cnt++;
        }
        if(state != PatternState.ACTIVE && state != PatternState.COMPLETE)
        {
            state = PatternState.FAILED;
            fractal.status = FractalRecord.FractalStatus.FAILED;
        }
        
        return rc;
    }

    boolean findCompletePattern_BC_impl(QuoteSection section, FractalRecord frac, PatternState state)
    {
        
        // TODO
        boolean rc = false;
        if(section.size() > 3)
        {
            Quote max = section.getMax(); // section starts at B
            // max is not first (B) but can be last
             if( max.getDay() > section.first().getDay() && max.getDay() <= section.last().getDay())
             {
                 // max is not the start but may be the last
                 if(max.getAverage() >= frac.A.getAverage()) // require up lightning pattern C >= A
//                 if(true)
                 {
                    
                    if(section.getMinOn(section.first(), max).getDay() == frac.B.getDay() )
                    {
                        // There is not a lower point between 'B' and 'C'  i.e. min == B     
                        state = PatternState.FOUND_C;
                        frac.C = max;
                        
                        rc = true;
                    }
                 }
                 
             }
        }
        
        return rc;
    }    
    
    boolean isDuplicate(FractalRecord prevFrac, FractalRecord currFrac)
    {
        boolean rc = true;
        if(prevFrac != null  )
        {
            if(!prevFrac.equals(currFrac))
            {
                rc = false;
            }
        }
        else
        {
            // first one
            rc = false;
        }
        return rc;
    }
    
}
