/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalfinder;

import java.text.DateFormat;
import java.util.Date;

/**
 *
 * @author lrisinger
 */
public class Quote
{
    public Quote(String date, String symbol, float open, float high, float low, float close)
    {
        DateFormat df = DateFormat.getInstance();
        try
        {
            this.date = df.parse(date);
            this.symbol = symbol;
            this.open = open;
            this.high = high;
            this.low = low;
            this.close = close;
            this.volume = 0;
            this.day = 0;
            computeAvg();
            
        } catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public Quote(Date date, String symbol, float open, float high, float low, float close, int volume)
    {
        this.date = date;
        this.symbol = symbol;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.day = 0;
        computeAvg();
    }

    final void computeAvg()
    {
//        average = (open + high + low +close)/4;
        average = (high + low)/2;
    }
    public Date getDate()
    {
        return date;
    }

    public String getSymbol()
    {
        return symbol;
    }

    public float getOpen()
    {
        return open;
    }

    public float getHigh()
    {
        return high;
    }

    public float getLow()
    {
        return low;
    }

    public float getClose()
    {
        return close;
    }

    public int getVolume()
    {
        return volume;
    }

    public float getAverage()
    {
        return average;
    }

    public int getDay() 
    {
        return day;
    }

    public void setDay(int day) 
    {
        this.day = day;
    }

    @Override
    public String toString() {
        return "Quote{" + "day=" + day + ", average=" + average + '}';
    }
    
    
    
    //# Date, Ticker, Open, High, Low, Close, Volume
    private Date date;
    private int day = 0;
    private String symbol;
    private float open;
    private float high;
    private float low;
    private float close;
    private int volume;
    private float average;
    
    
}
