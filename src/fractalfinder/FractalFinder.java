/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalfinder;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jfree.ui.RefineryUtilities;

/**
 *
 * @author lrisinger
 */
public class FractalFinder 
{

    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        
        System.out.println("quote_data_file: "+ args[0]);
        if(args.length < 1)
        {
            System.out.println("Usage:  FractalFinder quote_data_file_csv");
            System.exit(1);
        }


        System.out.println(System.getProperty("user.dir"));
//        File quoteFile = new File("/storage/emulated/0/AppProjects/fractalfinder/data/sp500hst.txt");
//        QuoteLoader quoteLoader = new QuoteLoader(quoteFile);
        File quoteFile = new File(args[0]);
        QuoteLoader quoteLoader = new QuoteLoader(quoteFile);
        
        quoteLoader.load();
        List<String> symbolList = quoteLoader.getSymbolList();
        System.out.println("Loaded "+symbolList.size()+" symbols ");
        
        List<FractalRecord> partial = new ArrayList<FractalRecord>();
        List<FractalRecord> complete = new ArrayList<FractalRecord>();
        List<FractalRecord> active = new ArrayList<FractalRecord>();
        List<FractalRecord> failed = new ArrayList<FractalRecord>();
        
        final GraphWindow demo = new GraphWindow( "Result Graph" );         
        Iterator<String> symbolIter = symbolList.iterator();
        int cnt = 0;
        // loop over stocks
        while(symbolIter.hasNext() && cnt < 1000)
        {
            String symbol = symbolIter.next();
//            System.out.println("Symbol: "+symbol );
            PatternFinder finder = new PatternFinder();
            finder.search2(quoteLoader.getQuoteList(symbol));
            partial.addAll(finder.getPartial()); 
            complete.addAll(finder.getComplete());
            active.addAll(finder.getActive());
            failed.addAll(finder.getFailed());
	  
            System.out.println("Symbol: "+symbol +" Totals:  partial: "+ partial.size() + " compplete: "+complete.size() 
                                + " failed: "+failed.size() + " active: "+active.size());
            if(symbol.equals("VTR") )
            {
                demo.setQuoteList(quoteLoader.getQuoteList(symbol));
                for(FractalRecord fr: partial)
                {
                    demo.add(fr);
                }
                for(FractalRecord fr: complete)
                {
                    demo.add(fr);
                }
                for(FractalRecord fr: failed)
                {
                    demo.add(fr);
                }
                for(FractalRecord fr: active)
                {
                    demo.add(fr);
                }
                
                
                demo.drawGraph();
            }
            
            
            partial.clear();
            complete.clear();
            active.clear();
            failed.clear();
            
            cnt++;
        }
        
        
        
        
        System.out.println("-----------EXIT----------------");
        
        
        demo.pack();         
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible( true );
    }
}
