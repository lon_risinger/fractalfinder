/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalfinder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author lrisinger
 */
public class QuoteSection
{

    public QuoteSection()
    {
         this.list = new ArrayList();
    }
    
    public void add(Quote quote)
    {
        int index = list.size();
        list.add(quote);
        
        if(list.get(max).getAverage() < quote.getAverage())
        {
            max = index;
        }
        if(list.get(min).getAverage() > quote.getAverage())
        {
            min = index;
        }
        
    }
    public int size() { return list.size(); }
    /**
     * Get max quote
     * @return Quote with max average value over the whole list 
     */
    public Quote getMax() { return list.get(max);  }
    /**
     * Get min quote
     * @return Quote with min average value over the whole list 
     */
    public Quote getMin() { return list.get(min);  }
    
    /**
     * Get min quote between two quotes including those quotes
     * @param x - start quote
     * @param y - end quote - after x
     * @return 
     */
    public Quote getMinOn(Quote x, Quote y)
    {
        Quote min = x;
        int start = list.indexOf(x);
        int end = list.indexOf(y);
        for(int i = start ; i <= end; i++)
        {
            Quote tmp = list.get(i);
            if(tmp.getAverage() < min.getAverage())
            {
                min = tmp;
            }
        }
        return min;
    }
    /**
     * Get max quote between two quotes including those quotes
     * @param x - start quote
     * @param y - end quote - after x
     * @return 
     */
    public Quote getMaxOn(Quote x, Quote y)
    {
        Quote max = x;
        int start = list.indexOf(x);
        int end = list.indexOf(y);
        for(int i = start ; i <= end; i++)
        {
            Quote tmp = list.get(i);
            if(tmp.getAverage() > max.getAverage())
            {
                max = tmp;
            }
        }
        return max;
    }
    public Quote last() { return list.get(list.size() - 1); }
    public Quote first() { return list.get(0); }
    
    public void clear()
    {
        list.clear();
        max = min = 0;
    }
    
    private int max = 0; // index of max
    private int min = 0; // index of min
    
    private List<Quote> list = null;
    
}
