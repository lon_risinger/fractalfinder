This application is a based on concepts found in "Fractals and Scaling in Finance" by Benoit Mandelbrot

It attempts to find fractal patterns in historic S&P 500 stock data.  This is accomplished by finding an initial 
fractal genartor pattern, i.e. a lightning bolt up pattern, then scaling up one fractal level and searching for 
the resultant larger fractal pattern within the data starting at the initial starting point of the generator.  Results 
are graphed per selected stock. If these patterns existed within the data, they might allow one to make high probablity 
short term price predictions.  Unfortunately, these patterns don't seem to exist.  Very few found generator patterns result
in existing larger patterns. But its still an interesting excersize in pattern matching and makes pretty graphs.